const hubspot = require("@hubspot/api-client");
const { twilioClient, getWorkers } = require("./twilio");
const config = require("../config");
const hubspotClient = new hubspot.Client({ apiKey: config.hubspot.api_key });
const hubspotContactProperties = [
  "phone",
  "mobilephone",
  "email",
  "firstname",
  "lastname",
  "avatar",
  "worker",
];

// Map between customer address and worker identity
// Used to determine to which worker route a new conversation with a particular customer
//
// {
//     customerAddress: workerIdentity
// }
//
// Example:
//     {
//         'whatsapp:+12345678': 'john@example.com'
//     }
// const customersToWorkersMap = {};

// Customers list
// Example:
// [
//   {
//      customer_id: 98,
//      display_name: 'Bobby Shaftoe',
//      channels: [
//          { type: 'email', value: 'bobby@example.com' },
//          { type: 'sms', value: '+123456789' },
//          { type: 'whatsapp', value: 'whatsapp:+123456789' }
//      ],
//      links: [
//          { type: 'Facebook', value: 'https://facebook.com', display_name: 'Social Media Profile' }
//      ],
//      details:{
//          title: "Information",
//          content: "Status: Active" + "\n\n" + "Score: 100"
//      },
//      worker: 'john@example.com'
//   }
// ]

// const customers = [];

const parseContact = (contact) => {
  const prop = contact.properties;

  return {
    customer_id: contact.id,
    display_name: `${prop.firstname} ${prop.lastname}`,
    worker: prop.worker,
    avatar: prop.avatar,
    channels: ["email", "sms", "whatsapp"]
      .map((type) => {
        const phone = prop.phone || prop.mobilephone;
        let value = null;

        switch (type) {
          case "email":
            value = prop.email;
            break;
          case "sms":
            value = phone;
            break;
          case "whatsapp":
            value = phone ? `whatsapp:${phone}` : null;
            break;
        }

        return value ? { type, value } : null;
      })
      .filter(Boolean),
  };
};

const getCustomers = async (workerIdentity) => {
  const result = await hubspotClient.crm.contacts.searchApi.doSearch({
    filterGroups: [
      {
        filters: [
          {
            propertyName: "worker",
            operator: "EQ",
            value: workerIdentity,
          },
        ],
      },
    ],
    properties: hubspotContactProperties,
  });

  return result.body.results.map(parseContact);
};

const findWorkerForCustomer = async (customerNumber) => {
  const result = await hubspotClient.crm.contacts.searchApi.doSearch({
    query: customerNumber,
    properties: hubspotContactProperties,
  });

  if (result.body.results.length <= 0) {
    return null;
  }

  return result.body.results[0].properties.worker;
};

const findRandomWorker = async () => {
  const workers = await getWorkers();
  const randomIndex = Math.floor(Math.random() * workers.length);

  return workers[randomIndex].identity;
};

const getCustomersList = async (workerIdentity, pageSize, anchor) => {
  const workerCustomers = await getCustomers(workerIdentity);
  const list = workerCustomers.map((customer) => ({
    display_name: customer.display_name,
    customer_id: customer.customer_id,
    avatar: customer.avatar,
  }));

  if (!pageSize) {
    return list;
  }

  if (anchor) {
    const lastIndex = list.findIndex(
      (c) => String(c.customer_id) === String(anchor)
    );
    const nextIndex = lastIndex + 1;
    return list.slice(nextIndex, nextIndex + pageSize);
  } else {
    return list.slice(0, pageSize);
  }
};

const getCustomerByNumber = async (customerNumber) => {
  const result = await hubspotClient.crm.contacts.searchApi.doSearch({
    query: customerNumber,
    properties: hubspotContactProperties,
  });

  if (result.body.results.length <= 0) {
    return null;
  }

  return parseContact(result.body.results[0]);
};

const getCustomerById = async (customerId) => {
  try {
    const result = await hubspotClient.crm.contacts.basicApi.getById(
      customerId,
      hubspotContactProperties
    );
    return parseContact(result.body);
  } catch {
    return null;
  }
};

module.exports = {
  //   customersToWorkersMap,
  findWorkerForCustomer,
  findRandomWorker,
  getCustomerById,
  getCustomersList,
  getCustomerByNumber,
};

// (async () => {
//
// })();
