const config = require("../config");
const twilioClient = require("twilio")(
  config.twilio.account_sid,
  config.twilio.auth_token
);
let workers = [];

const getWorkers = async () => {
  if (workers.length > 0) {
    return workers;
  }

  workers = await Promise.all(
    config.twilio.worker_sids.map(
      async (sid) => await twilioClient.frontlineApi.v1.users(sid).fetch()
    )
  );

  return workers;

  // {
  //   sid: '',
  //   identity: '',
  //   friendlyName: '',
  //   avatar: null,
  //   state: 'active',
  //   url: 'https://frontline-api.twilio.com/v1/Users/USxxxxx'
  // }
};

module.exports = {
  getWorkers,
  twilioClient,
};
