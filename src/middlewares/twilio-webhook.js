const twilio = require('twilio');
const config = require('../config');

const twilioWebhookMiddleware = twilio.webhook(config.twilio.auth_token, {validate: config.environment != 'test'});

module.exports = twilioWebhookMiddleware;
